package dk.itu.mario.level.generator;

import dk.itu.mario.gpllevel.EvolutionaryAlgorithm;
import dk.itu.mario.gpllevel.GPLMarioLevel;
import dk.itu.mario.MarioInterface.GamePlay;
import dk.itu.mario.MarioInterface.LevelGenerator;
import dk.itu.mario.MarioInterface.LevelInterface;

import java.util.Scanner;

public class CustomizedLevelGenerator implements LevelGenerator
{
	public LevelInterface generateLevel()
	{
		LevelInterface level = new GPLMarioLevel(launchMenu());

		return level;
	}

	private static String[] launchMenu()
	{
		Scanner reader = new Scanner(System.in);
		int option = 0;
		EvolutionaryAlgorithm evo;
		Boolean wasRunOnce = false;

		while (true)
		{
			System.out.println("Select an option:\n" +
					"1: Run the Evolutionary Algorithm\n" +
					"2: Launch a Mario Game with the last Best Level Stored\n" +
					"3: Launch a Mario Game with the Level used to establish the fitness Ratios\n" +
					"4: Exit");

			option = reader.nextInt();

			switch (option)
			{
				case 1:
					evo = new EvolutionaryAlgorithm();
					evo.train();
					wasRunOnce = true;
					break;
				case 2:
					if (!wasRunOnce) System.out.println("Evolutionary Algorithm not executed. Launching a previously saved map.\n");
					else System.out.println("Launching last generated map.\n");
					return EvolutionaryAlgorithm.getSavedPhenotypeAsArray("BestDerivation.txt");
				case 3:
					System.out.println("Launching the standard fitness map.");
					return EvolutionaryAlgorithm.getSavedPhenotypeAsArray("StandardFitnessMap.txt");
				case 4:
					System.out.println("Exiting.\n");
					return null;
				default:
					System.out.println("Not a valid option.\n");
					break;
			}
		}
	}

	public LevelInterface generateLevel(GamePlay playerMetrics) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public LevelInterface generateLevel(String detailedInfo) {
		// TODO Auto-generated method stub
		return null;
	}

}
