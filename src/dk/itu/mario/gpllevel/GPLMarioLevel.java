package dk.itu.mario.gpllevel;

import dk.itu.mario.engine.sprites.SpriteTemplate;
import dk.itu.mario.level.MarioLevel;
import grammar.Derivation;
import grammar.Grammar;
import grammar.GrammarException;

import java.util.Random;

/**
 * gpllevel Mario Level Generator
 * Created by manuel.cabeza on 12/01/2016.
 */
public class GPLMarioLevel extends MarioLevel
{
    public static int INIT_X = 5;

    private Random rng;
    private int x;
    private int previousX;
    private String[] levelPhenotype;

    public GPLMarioLevel(String[] levelPhenotype)
    {
        super(20, EvolutionaryAlgorithm.getPhenotypeMapLength(levelPhenotype));

        this.levelPhenotype = levelPhenotype;
        rng = new Random();
        x = INIT_X;

        generateLevel();
    }

    public void generateLevel()
    {
        int size = levelPhenotype.length;
        System.out.println("Phenotype of the Map:");
        for (int i = 0; i < size; ++i) System.out.printf(levelPhenotype[i] + ((i + 1 < size) ? "|" : ""));
        System.out.println();

        int globalX       = 5;
        int i             = 0;
        int x             = 0;
        int y             = 0;
        int width         = 0;
        int wb            = 0;
        int wa            = 0;
        int amount        = 0;
        int monstertype   = 0;
        int monsterwinged = 0;
        String mode       = "";
        String type       = "";

        while (i < size)
        {
            switch (levelPhenotype[i])
            {
                case "HCANNON":
                    globalX += x + wb;
                    x     = Integer.parseInt(levelPhenotype[++i]);
                    y     = Integer.parseInt(levelPhenotype[++i]);
                    width = Integer.parseInt(levelPhenotype[++i]);
                    wb    = Integer.parseInt(levelPhenotype[++i]);
                    wa    = Integer.parseInt(levelPhenotype[++i]);

                    buildHillCannon(globalX + x, y, width, wb, wa);
                    globalX += wa;
                    break;
                case "HTUBE":
                    globalX += x + wb;
                    x     = Integer.parseInt(levelPhenotype[++i]);
                    y     = Integer.parseInt(levelPhenotype[++i]);
                    width = Integer.parseInt(levelPhenotype[++i]);
                    wb    = Integer.parseInt(levelPhenotype[++i]);
                    wa    = Integer.parseInt(levelPhenotype[++i]);

                    buildHillTube(globalX + x, y, width, wb, wa);
                    globalX += wa;
                    break;
                case "PLATFORM":
                    globalX += x + (width / 2);
                    x     = Integer.parseInt(levelPhenotype[++i]);
                    y     = Integer.parseInt(levelPhenotype[++i]);
                    width = Integer.parseInt(levelPhenotype[++i]);
                    mode  = levelPhenotype[++i];

                    buildPlatform(globalX + x, y, width, mode.toLowerCase());
                    globalX += (width / 2);
                    break;
                case "GAP":
                    /*
                    x     = Integer.parseInt(levelPhenotype[++i]);
                    y     = Integer.parseInt(levelPhenotype[++i]);
                    width = Integer.parseInt(levelPhenotype[++i]);
                    wb    = Integer.parseInt(levelPhenotype[++i]);
                    wa    = Integer.parseInt(levelPhenotype[++i]);

                    buildGaps(globalX + x, -1, width, wb, wa);
                    globalX += x + wa + wb;
                    i+=5;
                    */
                    break;
                case "CANNON":
                    globalX += x + 1;
                    x     = Integer.parseInt(levelPhenotype[++i]);
                    y     = Integer.parseInt(levelPhenotype[++i]);
                    width = Integer.parseInt(levelPhenotype[++i]);
                    wb    = Integer.parseInt(levelPhenotype[++i]);
                    wa    = Integer.parseInt(levelPhenotype[++i]);

                    buildCannons(globalX + x, y, width, wb, wa);
                    globalX += 1;
                    break;
                case "HILL":
                    globalX += x + (width / 2);
                    x     = Integer.parseInt(levelPhenotype[++i]);
                    y     = Integer.parseInt(levelPhenotype[++i]);
                    width = Integer.parseInt(levelPhenotype[++i]);

                    buildHillStraight(globalX + x, y, width);
                    globalX += (width / 2);
                    break;
                case "TUBE":
                    globalX += x + (wb / 2);
                    x     = Integer.parseInt(levelPhenotype[++i]);
                    y     = Integer.parseInt(levelPhenotype[++i]);
                    width = Integer.parseInt(levelPhenotype[++i]);
                    wb    = Integer.parseInt(levelPhenotype[++i]);
                    wa    = Integer.parseInt(levelPhenotype[++i]);

                    buildTubes(globalX + x, y, width, wb, wa);
                    globalX += wa;
                    break;
                case "COIN":
                    x      = Integer.parseInt(levelPhenotype[++i]);
                    y      = Integer.parseInt(levelPhenotype[++i]);
                    amount = Integer.parseInt(levelPhenotype[++i]);

                    for (int iteratorX = x; iteratorX < x + amount; ++iteratorX) setBlock(globalX + iteratorX, super.height - 1 - y, COIN);
                    globalX += x + (amount / 2);
                    break;
                case "MONSTER":
                    x             = Integer.parseInt(levelPhenotype[++i]);
                    y             = 14;
                    monstertype   = Integer.parseInt(levelPhenotype[++i]);
                    monsterwinged = Integer.parseInt(levelPhenotype[++i]);

                    setSpriteTemplate(globalX + x, y, new SpriteTemplate(monstertype, monsterwinged == 1));
                    if (rng.nextInt(3) == 0) --globalX;
                    break;
                case "BLOCK":
                    x             = Integer.parseInt(levelPhenotype[++i]);
                    y             = Integer.parseInt(levelPhenotype[++i]);
                    amount        = Integer.parseInt(levelPhenotype[++i]);
                    type          = levelPhenotype[++i];

                    for (int iteratorX = x; iteratorX < x + amount; ++iteratorX)
                    {
                        Byte blockType = bytifyStringGPL(type);
                        if (blockType == EMPTY) System.out.println("Error reading blockType.");

                        setBlock(globalX + iteratorX, super.height - 2 - y, blockType);
                        if (blockType == BLOCK_POWERUP) break;
                    }

                    globalX += x + (amount / 2);
                    break;
                case "SPAWN":
                case "GOAL":
                case "NOTHING":
                default:
                    break;
            }

            ++i;
        }

        System.out.println("Game Ready!");
    }

    public static byte bytifyStringGPL(String blockType)
    {
        switch(blockType)
        {
            case "EMPTYBLOCK":
                return BLOCK_EMPTY;
            case "POWERUPBLOCK":
                return BLOCK_POWERUP;
            case "COINBLOCK":
                return BLOCK_COIN;
            default:
                return EMPTY;
        }
    }
}
