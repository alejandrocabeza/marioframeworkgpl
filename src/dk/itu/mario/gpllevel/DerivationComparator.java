package dk.itu.mario.gpllevel;

import grammar.Derivation;

import java.util.Comparator;

/**
 * Comparator to sort Derivations
 * Created by Strinnityk on 07/02/2016.
 */
public class DerivationComparator implements Comparator<Derivation>
{
    @Override
    public int compare(Derivation a, Derivation b) { return a.fitness< b.fitness ? -1 : a.fitness == b.fitness ? 0 : 1; }
}