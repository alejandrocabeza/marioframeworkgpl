package dk.itu.mario.gpllevel;

import com.sun.org.apache.bcel.internal.generic.POP;
import dk.itu.mario.engine.sprites.SpriteTemplate;
import grammar.Derivation;
import grammar.Grammar;
import grammar.GrammarException;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/**
 * Evolutionary Algorithm
 * Created by Strinnityk on 07/02/2016.
 */
public class EvolutionaryAlgorithm
{
    //CONSTANTS
    private static final int POPULATION_SIZE    = 100;
    private static final int TARGET_FITNESS     = 30;
    private static final int REPLACE_PERCENTAGE = 60;
    private static final int MUTATION_CHANCE    = 10;
    private static final int MAX_DEPTH          = 100;
    private static final int MAX_GENERATIONS    = 1000;

    //FITNESS RATIOS
    private static final float THREAT_RATIO        = 0.07f;
    private static final float ACTION_RATIO        = 0.06f;
    private static final float BACKGROUND_RATIO    = 0.03f;
    private static final float INTERACTUABLE_RATIO = 0.06f;


    //ATTRIBUTES

    //Grammar to generate individuals
    private Grammar levelGrammar;

    //Population
    private ArrayList<Derivation> population = new ArrayList<>();

    //Random Number Generator
    Random rng = new Random();


    //METHODS
    public EvolutionaryAlgorithm()
    {
        if (REPLACE_PERCENTAGE > 100 || REPLACE_PERCENTAGE < 0) System.exit(0);

        try
        {
            levelGrammar = new Grammar("src/dk/itu/mario/gpllevel/marioGrammar.txt");

            for (int i = population.size(); i < POPULATION_SIZE; ++i) population.add(new Derivation(levelGrammar, MAX_DEPTH));
        }
        catch (GrammarException e)
        {
            System.out.println("Grammar could not be loaded or derivations could not be created properly.");
        }

        evaluateGeneration();
    }

    private void evaluateGeneration()
    {
        for (Derivation individual : population)
        {
            if (individual.fitness != 0) continue;

            String[] phenotype = individual.getWord().split(",|;");

            int distance      = getPhenotypeMapLength(phenotype);

            int action        = 0;
            int interactuable = 0;
            int threat        = 0;
            int background    = 0;

            int size = phenotype.length;
            for (int i = 0; i < size; ++i)
            {
                if (EvolutionaryAlgorithm.isNumeric(phenotype[i])) continue;

                switch(phenotype[i])
                {
                    case "CANNON":
                    case "HCANNON":
                        ++threat;
                    case "TUBE":
                    case "HTUBE":
                        ++action;
                        break;
                    case "HILL":
                    case "PLATFORM":
                        ++background;
                        break;
                    case "BLOCK":
                    case "COIN":
                        ++interactuable;
                        break;
                    case "GAP":
                    case "MONSTER":
                        ++threat;
                        break;
                    case "SPAWN":
                    case "GOAL":
                    case "NOTHING":
                    default:
                        break;
                }
            }

            double distanceRatio      = Math.pow(Math.abs(150 - distance), 3);
            int distanceFit           = (distanceRatio < Integer.MAX_VALUE) ? (int)distanceRatio : Integer.MAX_VALUE;

            int actionFit        = getFit(action,        2, distance, ACTION_RATIO);
            int interactuableFit = getFit(interactuable, 3, distance, INTERACTUABLE_RATIO);
            int backgroundFit    = getFit(background,    2, distance, BACKGROUND_RATIO);
            int threatFit        = getFit(threat,        3, distance, THREAT_RATIO);

            individual.fitness = distanceFit + actionFit + interactuableFit + threatFit + backgroundFit;
        }
    }

    private int[] selectionByTournament()
    {
        int[] parents = new int[2];

        for (int i = 0; i < 2; ++i)
        {
            //As the individuals are sorted by fitness, there's no need for them to "fight"
            //This is as simple as throwing X random numbers and selecting the lowest (best) one

            int tournamentWinner = Integer.MAX_VALUE;
            for (int j = 0; j < 5; ++j)
            {
                int fighter = rng.nextInt(POPULATION_SIZE);
                if (fighter < tournamentWinner) tournamentWinner = fighter;
            }

            parents[i] = tournamentWinner;

            //If both parents are the same individual, the second parent is recalculated
            if ((i == 1) && (parents[1] == parents[0])) --i;
        }

        return parents;
    }

    private void produceNextGeneration()
    {
        int replace = POPULATION_SIZE * REPLACE_PERCENTAGE / 100;
        ArrayList<Derivation> offspring = new ArrayList<>();

        while (offspring.size() < replace)
        {
            try
            {
                int parents[] = selectionByTournament();
                ArrayList<Derivation> children = (ArrayList<Derivation>)population.get(parents[0]).crossoverWX(population.get(parents[1]));

                if (rng.nextInt(100) <= MUTATION_CHANCE) children.set(0, children.get(0).mutate());
                if (rng.nextInt(100) <= MUTATION_CHANCE) children.set(1, children.get(1).mutate());

                offspring.add(children.get(0));
                offspring.add(children.get(1));
            }
            catch (GrammarException e)
            {
                System.out.println("Error when producing next generation.");
            }
        }

        for (int i = 0; i < replace; ++i) population.remove(population.size() - 1);

        for (Derivation derivation : offspring)
        {
            if (population.size() >= POPULATION_SIZE) break;
            population.add(derivation);
        }
    }

    private void saveDerivationPhenotype(Derivation derivation)
    {
        try
        {
            FileWriter outFile = new FileWriter("BestDerivation.txt");
            try
            {
                PrintWriter out = new PrintWriter(outFile);
                out.print(derivation.getWord());
            }
            finally
            {
                outFile.close();
            }
        }
        catch (IOException e)
        {
            System.out.println("Fail saving gene.");
        }
    }

    private static String loadDerivationPhenotype(String filename)
    {
        String phenotype = "";

        try
        {
            phenotype = new Scanner(new File(filename)).useDelimiter("\\Z").next();
        }
        catch (IOException e)
        {
            System.out.println("Error loading the best derivation.");
        }

        return phenotype;
    }

    public static String[] getSavedPhenotypeAsArray(String filename)
    {
        return loadDerivationPhenotype(filename).split(",|;");
    }

    public Derivation train()
    {
        int generationCount = 0;

        while(generationCount < MAX_GENERATIONS)
        {
            //Evaluation of the current generation
            this.evaluateGeneration();
            this.population.sort(new DerivationComparator());


            //Getting information about the current generation
            float minFitness       = population.get(POPULATION_SIZE - 1).fitness;
            String worstIndividual = population.get(POPULATION_SIZE - 1).getWord();
            float maxFitness       = population.get(0).fitness;
            System.out.println(generationCount + " " + maxFitness);
            String bestIndividual  = population.get(0).getWord();

            float avgFitness = 0.f;
            for (Derivation individual : population) avgFitness += individual.fitness;
            avgFitness /= POPULATION_SIZE;

            //Printing information about the current generation
            String output = "Generation: " + generationCount;
            output += "\t AvgFitness: " + avgFitness + "\n";
            output += "MinFitness: " + minFitness + " (" + worstIndividual + ")" + "\n";
            output += "MaxFitness: " + maxFitness + " (" + bestIndividual + ")" + "\n";
            System.out.println(output);

            if (maxFitness <= TARGET_FITNESS) break;

            this.produceNextGeneration();
            generationCount++;
        }

        Derivation bestDerivation = population.get(0);
        this.saveDerivationPhenotype(bestDerivation);

        String endOutput = "\n\nBestGene: Ending Generation - " + generationCount + " | ID - 0 | Fitness - " + bestDerivation.fitness + " | Phenotype - " + bestDerivation.getWord();
        System.out.println(endOutput + "\n");

        return bestDerivation;
    }

    public static void main(String[] args)
    {
        EvolutionaryAlgorithm evo = new EvolutionaryAlgorithm();
        evo.train();

    }

    public static boolean isNumeric(String str)
    {
        try
        {
            double d = Double.parseDouble(str);
        }
        catch (NumberFormatException e)
        {
            return false;
        }

        return true;
    }

    private int getFit(float attribute, int power, float distanceTotal, float ratio)
    {
        double proportion = Math.pow(Math.abs((ratio - ((attribute / distanceTotal))) * 1000), power);
        return (proportion < Integer.MAX_VALUE) ? (int)proportion : Integer.MAX_VALUE;
    }

    public static int getPhenotypeMapLength(String[] phenotype)
    {
        int x = GPLMarioLevel.INIT_X;

        int size = phenotype.length;
        for (int i = 0; i < size; ++i)
        {
            switch(phenotype[i])
            {
                case "HCANNON":
                    x += Integer.parseInt(phenotype[i + 1]) + (Integer.parseInt(phenotype[i + 3])) + Integer.parseInt(phenotype[i + 4]) + Integer.parseInt(phenotype[i + 5]);
                    break;
                case "HTUBE":
                    x += Integer.parseInt(phenotype[i + 1]) + (Integer.parseInt(phenotype[i + 3])) + Integer.parseInt(phenotype[i + 4])+ Integer.parseInt(phenotype[i + 5]);
                    break;
                case "PLATFORM":
                    x += Integer.parseInt(phenotype[i + 1]) + (Integer.parseInt(phenotype[i + 3]) / 2);
                    break;
                case "GAP":
                    //x += Integer.parseInt(phenotype[i + 1]) + (Integer.parseInt(phenotype[i + 3])) + Integer.parseInt(phenotype[i + 4])+ Integer.parseInt(phenotype[i + 5]);
                    break;
                case "CANNON":
                    x += Integer.parseInt(phenotype[i + 1]) + Integer.parseInt(phenotype[i + 5]);
                    break;
                case "HILL":
                    x += Integer.parseInt(phenotype[i + 1]) + (Integer.parseInt(phenotype[i + 3]) / 2);
                    break;
                case "TUBE":
                    x += Integer.parseInt(phenotype[i + 1]) + Integer.parseInt(phenotype[i + 5]);
                    break;
                case "COIN":
                    x += Integer.parseInt(phenotype[i + 1]) + (Integer.parseInt(phenotype[i + 3]) / 2);
                    break;
                case "MONSTER":
                    x += (new Random().nextInt(3) == 0) ? -1 : 0;
                    break;
                case "BLOCK":
                    x += Integer.parseInt(phenotype[i + 1]) + (Integer.parseInt(phenotype[i + 3]) / 2);
                    break;
                case "SPAWN":
                case "GOAL":
                case "NOTHING":
                default:
                    break;
            }
        }

        return x;
    }
}
